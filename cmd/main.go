package main

import (
	"github.com/casbin/casbin/v2"
	defaultrolemanager "github.com/casbin/casbin/v2/rbac/default-role-manager"
	"github.com/casbin/casbin/v2/util"
	"gitlab.com/testgit57/cronejob/send_email/api"
	"gitlab.com/testgit57/cronejob/send_email/config"
	"gitlab.com/testgit57/cronejob/send_email/email"
	"gitlab.com/testgit57/cronejob/send_email/pkg/logger"
	"gitlab.com/testgit57/cronejob/send_email/storage/postgres"
	"github.com/gomodule/redigo/redis"
	r "gitlab.com/testgit57/cronejob/send_email/storage/redis"

)

func main() {
	cfg := config.LoadConfig()
	log := logger.New(cfg.LogLevel, "send_email")
	emailSend := email.NewEmailSend(cfg, log)

	casbinEnforcer, err := casbin.NewEnforcer(cfg.AuthConfigPath, cfg.CSVFilePath)
	if err != nil {
		log.Error(`Error wiht casbin enforcer`, logger.Error(err))
		return 
	}

	err = casbinEnforcer.LoadPolicy()
	if err != nil {
		log.Error(`Error casbin load policy`, logger.Error(err))
		return
	}

	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc("keyMatch", util.KeyMatch)
	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc("keyMatch3", util.KeyMatch3)

	pool := &redis.Pool{
		MaxIdle: 10,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", cfg.RedisHost + ":" + cfg.RedisPort)
		},
	}

	postgres, err := postgres.NewPostgres(*cfg)
	if err != nil {
		log.Error("Error while connecting to postgres")
		panic(err)
	}
	defer postgres.DB.Close()
	
	server := api.NewRouter(
		api.Option{
			Conf: cfg,
			Logger: log,
			EmailSender: emailSend,
			CasbinEnforcer: casbinEnforcer,
			Redis: r.NewRedisRepo(pool),
			Postgres: postgres,
		},
	)
	if err := server.Run(cfg.Host + ":" + cfg.Port); err != nil {
		log.Fatal("failed to run the server", logger.Error(err))
		panic(err)
	}

}