CREATE TABLE IF NOT EXISTS messages (
    id uuid NOT NULL PRIMARY KEY,    
    news_id uuid NOT NULL REFERENCES news(id),
    first_name TEXT,
    last_name TEXT,
    email TEXT,
    scheduled_at  TIMESTAMP WITHOUT TIME ZONE,
    minutes_after int
);