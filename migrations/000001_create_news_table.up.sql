CREATE TABLE IF NOT EXISTS news (
    id uuid NOT NULL PRIMARY KEY,
    content TEXT,
    send_email TEXT, 
    send_email_password TEXT
);