run:
	go run cmd/main.go

swag:
	swag init -g api/router.go -o api/docs

migrate_up:
	migrate -path migrations/ -database postgres://citizenfour:12321@localhost:5432/send_email up

migrate_down:
	migrate -path migrations/ -database postgres://citizenfour:12321@localhost:5432/send_email down

migrate_force:
	migrate -path migrations/ -database postgres://citizenfour:12321@localhost:5432/send_email force 6
