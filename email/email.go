package email

import (
	"bytes"
	"html/template"
	"net/smtp"

	"gitlab.com/testgit57/cronejob/send_email/api/models"
	"gitlab.com/testgit57/cronejob/send_email/config"
	"gitlab.com/testgit57/cronejob/send_email/pkg/logger"
)

type EmailSend struct {
	Cfg    *config.Config
	Logger logger.Logger
}

type Message struct {
	FirstName string
	LastName  string
	News      string
}

func NewEmailSend(cfg *config.Config, log logger.Logger) *EmailSend {
	return &EmailSend{
		Cfg:    cfg,
		Logger: log,
	}
}

func (e *EmailSend) SendEmailToSupscribers(ecfg *models.SendEmailConfig, req *models.SendNewsToSupscribersReq) error {
	for _, el := range req.To {
		body := new(bytes.Buffer)
		t, err := template.ParseFiles("./email/html_templates/news.html")
		if err != nil {
			return err
		}
		mInfo := &Message{
			FirstName: el.FirstName,
			LastName:  el.LastName,
			News:      req.News,
		}

		t.Execute(body, mInfo)
		mime := "MIME-version: 1.0\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
		msg := []byte("Subject: Mailganer News\n" + mime + body.String())

		auth := smtp.PlainAuth("", ecfg.Email, ecfg.Password,"smtp.gmail.com")

		err = smtp.SendMail("smtp.gmail.com:587", auth, ecfg.Email, []string{el.Email}, msg)
		if err != nil {
			e.Logger.Error(`Error while sending email`, logger.Error(err))
			return err
		}
	}
	return nil

}
